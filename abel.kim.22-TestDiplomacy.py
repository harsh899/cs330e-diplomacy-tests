# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):

    # basic 
    def test_solve1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")
       
    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid \nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")
    
    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")
    
    def test_solve4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
  
    def test_solve5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")
    
    def test_solve6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve7(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")    

    # Test if 2 armies swap places
    def test_solve8(self):
        r = StringIO("A Tokyo Hold\nB NewYork Move Madrid\nC Madrid Move NewYork\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Tokyo\nB Madrid\nC NewYork\n")
    
    # Test if armies can move to a place not named yet
    def test_solve9(self):
        r = StringIO("A Madrid Hold\nB NewYork Move Thailand\nC Borneo Move Chicago\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Thailand\nC Chicago\n")
    
    # Test if armies support each other and get attacked die properly
    def test_solve10(self):
        r = StringIO("A Madrid Support B\nB NewYork Support A\nC Borneo Move Madrid\nD Thailand Move NewYork\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
        
    # Test if a supported army attacks support
    def test_solve11(self):
        r = StringIO("A Madrid Support B\nB Tokyo Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

if __name__ == "__main__":
    main()
      